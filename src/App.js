import React, {Component} from 'react';
import {StatusBar, SafeAreaView} from 'react-native';

import {Provider} from 'mobx-react';

import store from './store';
import RootNavigator from './routes';
import NavigationService from './services/NavigationService';
export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar hidden={true} />
        <Provider {...store}>
          <RootNavigator
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Provider>
      </SafeAreaView>
    );
  }
}
