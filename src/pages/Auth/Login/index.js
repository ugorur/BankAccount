import React, {Component} from 'react';
import {Container, Content, Item, Input, Button, Text} from 'native-base';

import {StyleSheet, TouchableOpacity, TouchableHighlight} from 'react-native';
import NavigationService from '../../../services/NavigationService';
import TouchID from 'react-native-touch-id';
export default class SuccessInputTextboxExample extends Component {
  navigateRegister = () => {
    NavigationService.navigate('Register');
  };

  componentDidMount() {
    this._pressHandler();
  }

  _pressHandler = () => {
    const optionalConfigObject = {
      title: 'Authentication Required', // Android
      imageColor: '#6B319A', // Android
      imageErrorColor: '#6B319A', // Android
      sensorDescription: 'Touch sensor', // Android
      sensorErrorDescription: 'Failed', // Android
      cancelText: 'Cancel', // Android
      fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
      unifiedErrors: false, // use unified error messages (default false)
      passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
    };
    TouchID.authenticate('', optionalConfigObject)
      .then(success => {
        NavigationService.navigate('SignedIn');
      })
      .catch(error => {});
  };

  render() {
    return (
      <Container style={{flex: 1}}>
        <Content contentContainerStyle={styles.container}>
          <TouchableOpacity style={styles.button} onPress={this._pressHandler}>
            <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  button: {
    backgroundColor: '#6B319A',
    paddingHorizontal: 100,
    borderRadius: 10,
    paddingVertical: 5,
  },
  text: {
    fontSize: 20,
    marginVertical: 5,
    color: 'white',
  },
});
