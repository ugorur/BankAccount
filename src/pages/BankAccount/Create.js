import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Button,
  Input,
  Label,
  Picker,
  Icon,
  Text,
} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';

import App from '../../static/data';
import NavigationService from '../../services/NavigationService';

import {inject, observer} from 'mobx-react';

@inject('BankAccount')
@observer
export default class FixedLabelExample extends Component {
  state = {
    selected: App[0].path,
    password: '',
    name: App[0].name,
    path: App[0].path,
    url: App[0].url,
  };
  static navigationOptions = {
    title: 'New Bank Account',
  };
  constructor(props) {
    super(props);
  }

  onValueChange2(value) {
    console.log(value);
    console.log();
    this.setState({
      selected: value,
      name: value.name,
      path: value.path,
      url: value.url,
    });
  }

  handleSubmit = () => {
    this.props.BankAccount.insert({
      bankname: this.state.name,
      password: this.state.password,
      packagename: this.state.path,
      createddate:new Date().getTime(),
      url: this.state.url,
    });
    NavigationService.navigate('Home');
  };
  handleChangePassword = password => {
    this.setState({password: password});
  };
  render() {
    return (
      <Container style={{flex: 1}}>
        <Content contentContainerStyle={styles.container}>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholder="Select your Application"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange2.bind(this)}>
              {App.map(item => {
                return <Picker.Item label={item.name} value={item} />;
              })}
            </Picker>
          </Item>
          <Item style={styles.item}>
            <Input
              secureTextEntry
              onChangeText={this.handleChangePassword}
              value={this.state.password}
              placeholder="Password"
            />
          </Item>
          <Button onPress={this.handleSubmit} style={styles.loginButton} block>
            <Text style={styles.loginText}>Save</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  item: {
    paddingVertical: 5,
    marginVertical: 10,
  },
  loginButton: {
    paddingVertical: 5,
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: '#6B319A',
  },
  loginText: {
    color: 'white',
  },
});
