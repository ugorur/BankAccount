import React, {Component} from 'react';
import {
  Container,
  Content,
  Item,
  Button,
  Input,

  Text,
} from 'native-base';
import {StyleSheet} from 'react-native';

import NavigationService from '../../services/NavigationService';

import {inject, observer} from 'mobx-react';

@inject('BankAccount')
@observer
export default class FixedLabelExample extends Component {
  state = {
    id: this.props.navigation.getParam('data').id,
    password: this.props.navigation.getParam('data').password,
  };

  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('data', 'Edit Bank Account').bankname,
    };
  };
  handleSubmit = () => {
    this.props.BankAccount.update(this.state.id, {
      password: this.state.password,
    });

    NavigationService.navigate('Home');
  };

  handleDelete = () => {
    this.props.BankAccount.delete(this.state.id);
    NavigationService.navigate('Home');
  };
  handleChangePassword = password => {
    this.setState({password: password});
  };
  render() {
    return (
      <Container style={{flex: 1}}>
        <Content contentContainerStyle={styles.container}>
          <Item style={styles.item}>
            <Input
              secureTextEntry
              onChangeText={this.handleChangePassword}
              value={this.state.password}
              placeholder="Password"
            />
          </Item>
          <Button onPress={this.handleSubmit} style={styles.loginButton} block>
            <Text style={styles.loginText}>Update</Text>
          </Button>
          <Button onPress={this.handleDelete} style={styles.deleteButton} block>
            <Text style={styles.loginText}>Delete</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  item: {
    paddingVertical: 5,
    marginVertical: 10,
  },
  loginButton: {
    paddingVertical: 5,
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: '#6B319A',
  },
  deleteButton: {
    paddingVertical: 5,
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: '#D3382F',
  },
  loginText: {
    color: 'white',
  },
});
