import React, {Component} from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {Card, CardItem, Thumbnail, Text, Left, Body} from 'native-base';

export default class BankAccountItem extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{marginHorizontal: 10, marginVertical: 3}}
        onPress={() => {
          this.props.handleEdit();
        }}
        onLongPress={() => {
          this.props.handleOpen();
        }}>
        <Card style={{flex: 0}}>
          <CardItem>
            <Left>
              <Thumbnail
                square
                style={{width: 40, height: 40}}
                source={this.props.data.url}
              />
              <Body>
                <Text style={{fontSize: 18}}>{this.props.data.bankname}</Text>
                <Text style={{fontSize: 12}} note>
                  {new Date(this.props.data.createddate).toLocaleDateString()}{' '}
                  {new Date(this.props.data.createddate).toLocaleTimeString()}
                </Text>
              </Body>
            </Left>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }
}
