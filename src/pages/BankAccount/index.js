import React, {Component} from 'react';
import {ToastAndroid, Clipboard} from 'react-native';
import {Container, Content} from 'native-base';
import AccountItem from './components/BankAccountItem';
import Icon from 'react-native-vector-icons/FontAwesome5';
import NavigationService from '../../services/NavigationService';
import {inject, observer} from 'mobx-react';
var SendIntentAndroid = require('react-native-send-intent');

@inject('BankAccount')
@observer
export default class Home extends Component {
  static navigationOptions = {
    title: 'Account List',
    headerRight: () => (
      <Icon
        style={{marginRight: 15}}
        onPress={() => {
          NavigationService.navigate('AccountCreate');
        }}
        color="white"
        name="plus"
        size={28}
      />
    ),
  };

  handleOpenApp = data => {
    NavigationService.navigate('DetailAccount', {data: data});
  };

  handleEditablePage = (textMessage, path) => {
    ToastAndroid.showWithGravityAndOffset(
      textMessage,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
    setTimeout(() => {
      SendIntentAndroid.openApp(path);
    }, 1);
  };

  render() {
    return (
      <Container>
        <Content>
          {this.props.BankAccount.getList.map(r => {
            return (
              <AccountItem
                handleOpen={() => {
                  Clipboard.setString(r.password);
                  this.handleOpenApp(r);
                }}
                handleEdit={() => {
                  this.handleEditablePage(r.password, r.packagename);
                }}
                key={r.id}
                data={r}
              />
            );
          })}
        </Content>
      </Container>
    );
  }
}
