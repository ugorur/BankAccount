import HomePage from '../../pages/BankAccount';
import {createStackNavigator} from 'react-navigation-stack';
import CreatePage from '../../pages/BankAccount/Create';
import DetailPage from '../../pages/BankAccount/Detail';

export default createStackNavigator(
  {
    Home: {
      screen: HomePage,
    },
    AccountCreate: {
      screen: CreatePage,
    },
    DetailAccount: {
      screen: DetailPage,
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#6B319A',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);
