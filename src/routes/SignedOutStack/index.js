import {createSwitchNavigator} from 'react-navigation';
import LoginPage from '../../pages/Auth/Login';

export default createSwitchNavigator({
  Login: LoginPage,
});
