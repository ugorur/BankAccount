import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import SignedInStack from './SignedInStack';
import SignedOutStack from './SignedOutStack';

export default createAppContainer(
  createSwitchNavigator(
    {
      SignedOut: SignedOutStack,
      SignedIn: SignedInStack,
    },
    {
      initialRouteName: 'SignedOut',
    },
  ),
);
