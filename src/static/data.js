let apps = [
  {
    name: 'Akbank',
    path: 'com.akbank.android.apps.akbank_direkt',
    url: require('../static/images/com.akbank.android.apps.akbank_direkt.png'),
  },
  {
    name: 'Bonus Flash',
    path: 'com.garanti.bonusapp',
    url: require('../static/images/com.garanti.bonusapp.png'),
  },
  {
    name: 'Denizbank',
    path: 'com.denizbank.mobildeniz',
    url: require('../static/images/com.denizbank.mobildeniz.png'),
  },
  {
    name: 'Enpara',
    path: 'finansbank.enpara',
    url: require('../static/images/finansbank.enpara.png'),
  },
  {
    name: 'Finansbank',
    path: 'com.finansbank.mobile.cepsube',
    url: require('../static/images/com.finansbank.mobile.cepsube.png'),
  },
  {
    name: 'Garanti',
    path: 'com.garanti.cepsubesi',
    url: require('../static/images/com.garanti.cepsubesi.png'),
  },

  {
    name: 'HSBC',
    path: 'tr.com.hsbc.hsbcturkey',
    url: require('../static/images/tr.com.hsbc.hsbcturkey.png'),
  },
  {
    name: 'Halkbank',
    path: 'com.tmobtech.halkbank',
    url: require('../static/images/com.tmobtech.halkbank.png'),
  },
  {
    name: 'ING Bank',
    path: 'com.ingbanktr.ingmobil',
    url: require('../static/images/com.ingbanktr.ingmobil.png'),
  },
  {
    name: 'İş Bankası',
    path: 'com.pozitron.iscep',
    url: require('../static/images/com.pozitron.iscep.png'),
  },
  {
    name: 'TEB',
    path: 'com.teb',
    url: require('../static/images/com.teb.png'),
  },
  {
    name: 'Vakıfbank',
    path: 'com.vakifbank.mobile',
    url: require('../static/images/com.vakifbank.mobile.png'),
  },
  {
    name: 'Yapıkredi',
    path: 'com.ykb.android',
    url: require('../static/images/com.ykb.android.png'),
  },
  {
    name: 'Ziraat',
    path: 'com.ziraat.ziraatmobil',
    url: require('../static/images/com.ziraat.ziraatmobil.png'),
  },
];
export default apps;
