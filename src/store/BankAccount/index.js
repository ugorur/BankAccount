import {observable, action, computed} from 'mobx';
import Data from '../../database';
class BankAccountStore {
  @observable bankAccountlist = [];
  service = new Data();
  constructor() {
    this.service.init();
    this.service.createTable('bankaccount', [
      {
        name: 'id',
        dataType: 'integer',
        isNotNull: true,
        options: 'PRIMARY KEY AUTOINCREMENT',
      },
      {
        name: 'bankname',
        dataType: 'text',
      },
      {
        name: 'createddate',
        dataType: 'integer',
      },
      {
        name: 'password',
        dataType: 'text',
      },
      {
        name: 'url',
        dataType: 'text',
      },
      {
        name: 'packagename',
        dataType: 'text',
      },
    ]);
    this.list();
  }
  @action async list() {
    this.bankAccountlist = await this.service.select('bankaccount');
  }
  @action async insert(bankData) {
    this.service.insert('bankaccount', bankData);
    this.list();
  }
  @action async delete(id) {
    this.service.delete('bankaccount', {id: id});
    this.list();
  }
  @action async update(id, newdata) {
    this.service.update('bankaccount', newdata, {id: id});
    this.list();
  }
  @computed get getList() {
    return this.bankAccountlist;
  }
}

export default new BankAccountStore();
